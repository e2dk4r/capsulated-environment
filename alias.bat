doskey ~=cd /d %HOME%
doskey cd=cd /d $*
doskey ls=dir /og $*
doskey cat=bat $*
doskey mv=move $*
doskey cp=xcopy $*
doskey rm=del /s /f /q $*
doskey kill=taskkill /f /t /im $1
doskey find=ag --silent -g $*

doskey vi=nvim $*
doskey vim=nvim $*
doskey editor=nvim $*
doskey git=hub $*

doskey youtube=mpv "ytdl://ytsearch5:$*"
doskey video=mpv $*
doskey audio=mpv --no-video --loop-playlist --shuffle $*