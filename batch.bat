@echo off
:: == UTF-8 ==
chcp 65001

:: == Variables ==
set "USR=%~dp0usr"
set "HOME=%~dp0home"
set "ETC=%~dp0etc"
set "TMP=%~dp0tmp"
set "TEMP=%TMP%"
set "PATH=%USR%\scoop\shims;%HOME%\scripts;%SystemRoot%\system32;%SystemRoot%\WindowsPowerShell\v1.0"
set "PATHEXT=.COM;.EXE;.BAT;.CMD"

set "HOMEPATH=%HOME%"
set "XDG_CONFIG_HOME=%HOME%\.config"
if exist "%~dp0env.bat" call "%~dp0env.bat"
if exist "%~dp0alias.bat" call "%~dp0alias.bat"

set "SSL_CERT_FILE=%USR%\curl\curl-ca-bundle.crt"

:: == PROMPT ==
title cenv
prompt ╔»[%username%@%computername%]═[$P]═[$T]$_╚»$S

:: == Launcher ==
if not exist "%HOME%" mkdir "%HOME%"
cd /d "%HOME%"

REM if exist "%USR%\cmder" (
REM 	start /realtime "cmder" "%USR%\cmder\Cmder.exe"
REM 	exit /b 0
REM )

if exist "%USR%\conemu" (
	set ConEmuDefaultCp=65001
	start /realtime "ConEmu64" "%USR%\conemu\ConEmu64.exe"
	exit /b 0
)

if exist "%USR%\clink" (
	start /realtime "clink" cmd /s /k "%USR%\clink\clink_x64.exe inject --profile %XDG_CONFIG_HOME%\clink"
	exit /b 0
)

call cmd /k
