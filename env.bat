@echo off
REM mpv
REM https://mpv.io/manual/stable/#environment-variables
set "MPV_HOME=%XDG_CONFIG_HOME%\mpv"

REM httpie
REM https://httpie.org/doc#config
set "HTTPIE_CONFIG_DIR=%XDG_CONFIG_HOME%\httpie"

REM SCOOP
REM https://github.com/lukesampson/scoop#install-scoop-to-a-custom-directory-by-changing-scoop
set "SCOOP=%USR%\scoop"
set "SCOOP_GLOBAL=%USR%\scoop"