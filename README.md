# Install
1. Copy this repo
2. Configure and install apps
3. Run startup script

# File System
| Path         | Description                 |
| ------------ | --------------------------- |
| usr          | User applications           | 
| home         | Home directory              |
| home\scripts | Various scripts             |
| tmp          | Temporary folder            |
| etc          | Various configuration files |
| batch.bat    | Shell startup script        |

# Recommended package manager
- [Scoop](https://github.com/lukesampson/scoop#installation)
# Recommended apps
- [7-Zip](https://www.7-zip.org/download.html)
- [aria2](https://github.com/aria2/aria2/releases)
- [curl](https://curl.haxx.se/download.html#Win64)
- [ffmpeg](https://ffmpeg.zeranoe.com/builds/win64/static/?C=M&O=D)
- [git](https://github.com/git-for-windows/git/releases/)
- [mpv](https://sourceforge.net/projects/mpv-player-windows/files/64bit/)
- [vscode](https://go.microsoft.com/fwlink/?Linkid=850640)
- [python](https://www.python.org/downloads/windows/)
    - download [get-pip.py](https://bootstrap.pypa.io/get-pip.py)
    - `python get-pip.py`
- youtube-dl
    - `pip install -U git+https://github.com/ytdl-org/youtube-dl.git`