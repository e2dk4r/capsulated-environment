@echo off
setlocal

:: == ARGUMENT PARSING ==
set /a "F_CLEAN=1"
set /a "F_FETCH=0"

:arg_loop
if /i "%1" neq "" (
    if /i "%1" equ "-f" (
        set /a "F_FETCH=1"
    )
    if /i "%1" equ "--force-fetch" (
        set /a "F_FETCH=1"
    )

    if /i "%1" equ "-d" (
        set /a "F_CLEAN=0"
    )
    if /i "%1" equ "--no-clean" (
        set /a "F_CLEAN=0"
    )
    shift
    goto :arg_loop
)

if /i "%1" equ "/f" (
    set /a "FORCE_FETCH=1"
    shift
)
if /i "%1" equ "/force" (
    set /a "FORCE_FETCH=1"
    shift
)

if /i "%1" equ "/nc" (
    set /a "CLEAN_UP=0"
    shift
)
if /i "%1" equ "/noclean" (
    set /a "CLEAN_UP=0"
    shift
)

:: == VARIABLES ==
set "URL=https://download.mozilla.org/?product=firefox-nightly-latest-ssl&os=win64&lang=en-US"
set "FIREFOX_ARCHIVE=%HOME%\firefox-nightly.7z"
set "FIREFOX_DIR=%USR%\firefox"

:: == REQUIREMENTS ==
call :exitUninstalled 7za 1
call :exitUninstalled curl 2

:: == ACTION ==
echo ***************************************
echo *** fetching latest firefox nightly ***

if /i "%F_FETCH%" equ "1" (
	echo *** forcing to fetch
    if exist "%FIREFOX_ARCHIVE%" (
		del "%FIREFOX_ARCHIVE%"
	)
)

if not exist "%FIREFOX_ARCHIVE%" (
    curl --location --output "%FIREFOX_ARCHIVE%" "%URL%"
) else (
    curl --continue-at - --location --output "%FIREFOX_ARCHIVE%" "%URL%"
)

echo ***************************************
echo *** extract to user applications    ***

call :waitClose "firefox.exe"

if exist "%FIREFOX_DIR%" ( rmdir /s /q "%FIREFOX_DIR%" )
7za x -t7z -o"%TMP%" "%FIREFOX_ARCHIVE%" "core\*"
move "%TMP%\core" "%FIREFOX_DIR%"

if /i "%F_CLEAN%" equ "1" (
    if exist "%FIREFOX_ARCHIVE%" (
        echo *** cleaning up remainings
        del "%FIREFOX_ARCHIVE%"
    )
)

echo ***************************************

endlocal
exit /b 0

:: == FUNCTIONS ==
:usage
echo NAME
echo    install-firefox - fetchs and install firefox
echo SYNOPSIS
echo    install-firefox [options]
echo DESCRIPTION
echo    install-firefox fetches and install firefox nightly x64 en-US on user
echo    applications.
echo
echo    options
echo        /nc, /noclean   - do not delete archive after extraction
echo        /f, /force      - force to fetch
goto :eof

:logError <msg: string>
echo error: %~1 2>&1
goto :eof

:exitUninstalled <program: string> <exitCode: int>
where %1 >nul 2>&1
if /i "%ERRORLEVEL%" NEQ "0" (
    call :logError "%1 not installed"
    exit /b %2
)
goto :eof

:waitClose <program: string>
tasklist /fo csv /fi "STATUS eq RUNNING" | find "%~1"
if /i "%ERRORLEVEL%" equ "0" (
    call :logError "%1 is running, please close"
    pause
    goto :waitClose
)
goto :eof