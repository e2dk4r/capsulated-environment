@echo off
set "EDITOR_BIN=%USR%\npp\notepad++.exe"

if not exist "%EDITOR_BIN%" (
    echo "%EDITOR_BIN" not exist
    exit /b 1
)

start /realtime "EDITOR" "%EDITOR_BIN%" %*