@echo off

:: == PRE ==
set /a argc=0
for %%a in (%*) do (
    set /a "argc+=1"
)

if /i "%ARGC%" equ "0" (
    call :usage
    exit /b 1
)

:: == ARGUMENT PARSING ==
set /a "F_RESULT=1"
set /a "F_ONLY_AUDIO=0"
set /a "F_RANDOM=0"

:arg_loop
if /i "%~1" neq "" (
    set "F_QUERY=%~1"

    if /i "%~1" equ "-n" (
        set /a "F_RESULT=%2"
        shift
    )
    if /i "%~1" equ "--result" (
        set /a "F_RESULT=%2"
        shift
    )
    if /i "%~1" equ "-a" (
        set /a "F_ONLY_AUDIO=1"
    )
    if /i "%~1" equ "--audio" (
        set /a "F_ONLY_AUDIO=1"
    )
    if /i "%~1" equ "-r" (
        set /a "F_RANDOM=1"
    )
    if /i "%~1" equ "--random" (
        set /a "F_RANDOM=1"
    )
    shift
    goto :arg_loop
)
:: == REQUIREMENTS ==
call :exitUninstalled mpv 2
call :exitUninstalled youtube-dl 3

:: == ACTION ==
set "YTDL_SEARCH=:%F_QUERY%"

if /i "%F_RESULT%" neq "1" (
    set "YTDL_SEARCH=%F_RESULT%%YTDL_SEARCH%"
)

if /i "%F_ONLY_AUDIO%" equ "1" (
    set "MPV_EXTRA_OPTS=%MPV_EXTRA_OPTS% --no-video"
)

if /i "%F_ONLY_AUDIO%" equ "1" (
    set "MPV_EXTRA_OPTS=%MPV_EXTRA_OPTS% --shuffle"
)

mpv %MPV_EXTRA_OPTS% "ytdl://ytsearch%YTDL_SEARCH%"
exit /b 0

:: == FUNCTIONS ==
:usage
echo NAME
echo    youtube - search youtube videos
echo SYNOPSIS
echo    youtube [options] query
echo DESCRIPTION
echo    youtube is a batch script for searching videos at youtube
echo    options
echo        -n, --result [number]   - a specified number of results for a query
echo        -a, --audio             - only audio
echo        -r, --random            - play files in random order.
echo    query
echo        query string for search
echo EXAMPLES
echo    youtube -n 5 "Kurzgesagt – In a Nutshell"
echo        search and play with 5 result
echo    youtube -a "Lana Del Rey"
echo        search and play with first result
echo    youtube --audio --result 20 "Lana Del Rey"
echo        search and play with first result
goto :eof

:exitUninstalled <program> <error_code>
where %1 >nul 2>&1
if /i "%ERRORLEVEL%" NEQ "0" (
    echo %1 not installed
    exit /b %2
)
goto :eof