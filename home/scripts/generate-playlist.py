#/usr/bin/env python
import urllib.request
import json
import re, unicodedata
import os, sys, argparse

API_URL = 'https://www.lynda.com/ajax/player?courseId={id}&type=course'
CACHE_DIR = 'json-data'

def get_course(courseId):
    rs = ''
    with urllib.request.urlopen(API_URL.format(id=courseId)) as f:
        rs = f.read().decode('utf-8')
    return rs

def get_json(courseId):
    filename = os.path.join(CACHE_DIR, '{id}.json'.format(id=courseId))

    if not os.path.exists(CACHE_DIR):
        os.mkdir(CACHE_DIR)

    # is it cached?
    if os.path.exists(filename):
        if os.path.getsize(filename) > 1:
            with open(filename, 'r', encoding='utf-8') as fp:
                content = fp.read()
            return json.loads(content)

    # fetch
    response = get_course(courseId)
    if not response:
        print('Cannot fetch course')
        return None
    with open(filename, 'w', encoding='utf-8') as fp:
        fp.write(response)

    return json.dumps(response)

def slugify(value):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = str(value)
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)

def make_playlist(data, filename, **kwargs):
    output = kwargs['output']                         \
        .replace('%(playlist_id)s', str(data['ID']))  \
        .replace('%(playlist_title)s', data['Title']) 

    filename = filename                               \
        .replace('%(playlist_id)s', str(data['ID']))  \
        .replace('%(playlist_title)s', data['Title'])
    filename = '{filename}.m3u'.format(filename=slugify(filename))

    print(filename)
    template = output

    seq_counter = 1
    with open(filename, 'w', encoding='utf-8') as fp:
        for chapter in data['Chapters']:
            output = template
            output = output                                                  \
                .replace('%(chapter)s', chapter['Title'])                    \
                .replace('%(chapter_id)s', str(chapter['ID']))               \
                .replace('%(chapter_number)s', str(chapter['ChapterIndex']))
            
            template_chapter = output
            for video in chapter['Videos']:
                output = template_chapter
                output = output                                 \
                    .replace('%(id)s', str(video['ID']))        \
                    .replace('%(title)s', video['Title'])       \
                    .replace('%(filename)s', video['FileName']) \
                    .replace('%(ext)s', 'mp4')

                fp.write(output)
                fp.write('\n')

                # increment counter
                seq_counter += 1

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--id', type=int, help='course id that belongs to playlist')
    parser.add_argument('-o', '--output', help='output template (see youtube-dl docs)')
    parser.add_argument('-n', '--name', default='%(playlist_title)s', help='playlist name')

    args = parser.parse_args()

    data = get_json(args.id)
    if data is None:
        print('Could not find course')
        sys.exit(1)

    make_playlist(data, args.name, output=args.output)

if __name__ == '__main__':
    main()
