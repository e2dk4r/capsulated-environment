@echo off
setlocal

:: == VARIABLES ==
call :findInstance
set "FIREFOX_PROFILE_DIR=%HOME%\.mozilla\firefox"

:: == REQUIREMENTS ==
if not exist "%FIREFOX_PATH%" (
	call :logError "Firefox not installed"
	call :logError "	on: '%FIREFOX_PATH%'"
	exit /b 1
)

:: == ACTION ==
start /realtime "Firefox" "%FIREFOX_PATH%" -profile "%FIREFOX_PROFILE_DIR%" %*

endlocal
exit /b 0

:: == FUNCTIONS ==
:findInstance
for %%I in (
    "%PROGRAMFILES%\mozilla firefox\firefox.exe"
    "%PROGRAMFILES%\firefox beta\firefox.exe"
    "%PROGRAMFILES%\firefox nightly\firefox.exe"
    "%USR%\firefox\firefox.exe"
) do (
    if exist "%%I" (
        @set "FIREFOX_PATH=%%~I"
    )
)
goto :eof

:logError <msg: string>
echo error: %~1 2>&1
goto :eof