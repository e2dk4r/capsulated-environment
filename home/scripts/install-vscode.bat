@echo off
setlocal

:: == ARGUMENT PARSING ==
set /a "F_CLEAN=1"
set /a "F_FETCH=0"

:arg_loop
if /i "%1" neq "" (
    if /i "%1" equ "-f" (
        set /a "F_FETCH=1"
    )
    if /i "%1" equ "--force-fetch" (
        set /a "F_FETCH=1"
    )

    if /i "%1" equ "-d" (
        set /a "F_CLEAN=0"
    )
    if /i "%1" equ "--no-clean" (
        set /a "F_CLEAN=0"
    )
    shift
    goto :arg_loop
)

:: == VARIABLES ==
set "URL=https://go.microsoft.com/fwlink/?Linkid=850640"
set "VSCODE_ARCHIVE=%HOME%\vscode-insider.zip"
set "VSCODE_DIR=%USR%\vscode"

:: == REQUIREMENTS ==
call :exitUninstalled 7za 1
call :exitUninstalled curl 2

:: == ACTION ==
echo ***************************************************
echo *** fetching latest visual studio code insiders ***

if /i "%F_FETCH%" equ "1" (
	echo *** forcing to fetch
    if exist "%VSCODE_ARCHIVE%" (
		del "%VSCODE_ARCHIVE%"
	)
)

if not exist "%VSCODE_ARCHIVE%" (
	curl --location --output "%VSCODE_ARCHIVE%" "%URL%"
) else (
    curl --continue-at - --location --output "%VSCODE_ARCHIVE%" "%URL%"
)

echo ***************************************************
echo *** extract to user applications                ***

call :waitClose "Code - Insiders.exe"

if exist "%VSCODE_DIR%" ( rmdir /s /q "%VSCODE_DIR%" )
7za x -tzip -o"%VSCODE_DIR%" "%VSCODE_ARCHIVE%"

if /i "%F_CLEAN%" equ "1" (
    if exist "%VSCODE_ARCHIVE%" (
        echo *** cleaning up remainings
        del "%VSCODE_ARCHIVE%"
    )
)

endlocal
exit /b 0

:: == FUNCTIONS ==
:usage
echo NAME
echo    install-vscode - fetchs and install Visual Studio Code
echo SYNOPSIS
echo    install-vscode [options]
echo DESCRIPTION
echo    install-vscode fetches and install Visual Studio Code Insiders on user
echo    applications.
echo
echo    options
echo        -d, --no-clean      - do not delete archive after extraction
echo        -f, --force-fetch   - force to fetch
goto :eof

:logError <msg: string>
echo error: %~1 2>&1
goto :eof

:waitClose <program: string>
tasklist /fo csv /fi "STATUS eq RUNNING" | find "%~1"
if /i "%ERRORLEVEL%" equ "0" (
    call :logError "%1 is running, please close"
    pause
    goto :waitClose
)
goto :eof

:exitUninstalled <program: string> <exitCode: int>
where %1 >nul 2>&1
if /i "%ERRORLEVEL%" NEQ "0" (
    call :logError "%1 not installed"
    exit /b %2
)
goto :eof