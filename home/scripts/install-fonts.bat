@echo off
SETLOCAL EnableDelayedExpansion

:: requirements
if not exist "%USR%\share\fonts" (
	echo font directory not found
	echo 	at: "%USR%\share\fonts"
	exit /b 1
)

:: change directory
pushd "%USR%\share\fonts"

:: install fonts
setlocal
for /f "delims=" %%i in ('dir /b') do (
	set "font_file=%%i"
	set "font_name=!font_file:~,-4! (TrueType)"
	
	copy "!font_file!" "%WINDIR%\Fonts\"
	reg add "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts" /v "!font_name!" /t REG_SZ /d "!font_file!" /f
)
endlocal

:: go back
popd

exit /b 0