@echo off
setlocal
set "VSCODE_DIR=%USR%\vscodium"
set "VSCODE_EXTENSIONS_DIR=%HOME%\.vscode\extensions"
set "VSCODE_USER_DATA_DIR=%HOME%\.vscode\user-data"

set VSCODE_DEV=
set ELECTRON_RUN_AS_NODE=1

start /realtime "vscode" "%VSCODE_DIR%\vscodium.exe" "%VSCODE_DIR%\resources\app\out\cli.js" --extensions-dir "%VSCODE_EXTENSIONS_DIR%" --user-data-dir "%VSCODE_USER_DATA_DIR%" %*
endlocal