@echo off
setlocal

:: == VARIABLES ==
set "USERNAME="
set "PASSWORD="
set "PORT="
set "PROXY="
set "YTDL_OUTPUT=%%(playlist_title)s\%%(filename)s.%%(ext)s"

:: == REQUIREMENTS ==
call :exitUninstalled youtube-dl 1
call :exitPortClosed %PORT% 2

:: == ACTION ==
youtube-dl --username "%USERNAME%" --password "%PASSWORD%" --console-title --output "%YTDL_OUTPUT%" --add-metadata --all-subs %*
endlocal
exit /b 0

:: == FUNCTIONS ==
:usage
echo NAME
echo    lynda - download videos from lynda.com
echo SYNOPSIS
echo    lynda [ytdl_opts] url
echo DESCRIPTION
echo    lynda downloads videos from lynda.com using your username and password
echo    ytdl_opts
echo        extra youtube-dl options
echo    url
echo        course link to download
goto :eof

:logError <msg>
echo error: %~1 2>&1
goto :eof

:exitPortClosed <port_number> <exitCode>
netstat -anp TCP | find "%1" >nul 2>&1
if /i "%ERRORLEVEL%" NEQ "0" (
    call :logError "Is port %1 open?"
    exit /b %2
)
goto :eof

:exitUninstalled <program> <exitCode>
where %1 >nul 2>&1
if /i "%ERRORLEVEL%" NEQ "0" (
    call :logError "%1 not installed"
    exit /b %2
)
goto :eof