@echo off
setlocal
set "BROWSER_BIN=%USR%\qutebrowser\qutebrowser.exe"

if not exist "%BROWSER_BIN%" (
    echo %BROWSER_BIN% not exist
    exit /b 1
)

start "Browser" /d "%HOME%" /realtime "%BROWSER_BIN%" %*
endlocal